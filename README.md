
This repo is just a dumping ground to have patches we can then reference in different composer managed projects (Typically D8 sites).

# How to create a patch

 * Create a temporary working folder.
 * Download the official project into this folder. You can git clone if you want, this will save some time if you do.
	 * If the folder isn't already a git repo enter project folder and type: `git init` then `git add --all .` followed by `git commit -m "base commit"` this will create a git repo, add all files and commit them. This will give you a clean working base to now add your modifications to.
 * Enter project folder (which should now be a git repo that's just the pure, current version on the project) and now make any modifications you need.
 * To create the patch file type: `git diff > name_of_your.patch`
 * Move your patch file to this patch repo, git add and git commit it.

# How to use these patches

Once you have your patch in the repo you'll want to reference it in your code.

To get the path find the patch you want through the bitbucket web interface and click on it. 

Once viewing the patch use the three dots above the code next to the word "edit" and choose "open raw"

A new tab should open with a direct link to the patch which will look something like: 
https://bitbucket.org/monteconsulting/patches/raw/{git commit id}/name_of_your.patch

You can now use this URL in the patches section of your composer.json file.
	 